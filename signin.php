<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="form-top">
				<fieldset>
					<legend class="col-form-legend">Signin</legend>
					<p><label class="sr-only col-form-label">Username</label><input class="form form-control" type="text" name="username" placeholder="Username" id="username"></p>
					<p><label class="sr-only col-form-label">Password</label>
						<input class="form form-control" type="password" name="password" placeholder="password" id="password">
						<button onclick="passwordTogler('password', 'eye', 'txt')" id="siPswd" class="btn btn-warning" type="submit"><span id="txt">Show </span><i id="eye" class="fa fa-eye"></i></button>
					</p>
					<p><input class="btn btn-info btn-block" type="submit" name="masuk" value="signin" id="action" onclick="signin();"></p>
					<p>don't have acount ?,make one <a href="javaScript:void(0);" onclick="domChange('signup.php', 'GET', 'ajax')" title="singup">Singup</a></p>
				</fieldset>
			</div>
			<div id="signin">
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>